<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware('auth');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Route::get('/profile', 'ProfileController@index');

//Region Routes
Route::get('/area-management/regions', 'RegionController@index')->name('view.regions');
Route::delete('/delete/regions/{id}', 'RegionController@deleteRegion')->name('region.delete');
Route::get('/update/regions/{id}', 'RegionController@updateRegionForm')->name('region.update.form');
Route::put('/update/regions/{id}', 'RegionController@updateRegion')->name('regions.edit');
Route::post('/add/regions/', 'RegionController@addRegion')->name('region.store');
Route::get('/regions/form', 'RegionController@showRegionForm')->name('region.store.form');

//Assembly Routes
Route::get('/area-management/assemblies', 'AssemblyController@index')->name('view.assemblies');
Route::delete('/delete/assemblies/{id}', 'AssemblyController@deleteAssembly')->name('assembly.delete');
Route::get('/update/assemblies/{id}', 'AssemblyController@updateAssemblyForm')->name('assembly.update.form');
Route::put('/update/assemblies/{id}', 'AssemblyController@updateAssembly')->name('assembly.edit');
Route::post('/add/assemblies/', 'AssemblyController@addAssembly')->name('assembly.store');
Route::get('/assemblies/form', 'AssemblyController@showAssemblyForm')->name('assembly.store.form');

//Sub Metro Routes
Route::get('/area-management/submetros', 'SubMetroController@index')->name('view.submetros');
Route::delete('/delete/submetros/{id}', 'SubMetroController@deleteSubMetro')->name('submetro.delete');
Route::get('/update/submetros/{id}', 'SubMetroController@updateSubMetroForm')->name('submetro.update.form');
Route::put('/update/submetros/{id}', 'SubMetroController@updateSubMetro')->name('submetro.edit');
Route::post('/add/submetros', 'SubMetroController@addSubMetro')->name('submetro.store');
Route::get('/submetros/form', 'SubMetroController@showSubMetroForm')->name('submetro.store.form');

//System User Routes
Route::get('/area-management/user', 'UserController@index')->name('view.users');
Route::delete('/delete/user/{id}', 'UserController@deleteUser')->name('user.delete');
Route::get('/update/user/{id}', 'UserController@updateUserForm')->name('user.update.form');
Route::post('/update/user/{id}', 'UserController@updatedUser')->name('user.edit');
Route::post('/add/user', 'UserController@addUser')->name('user.store');
Route::get('/user/form', 'UserController@showUserForm')->name('user.store.form');

//Rating Residential Classes Routes
Route::get('/app-settings/res-class-mgmt', 'ResidentialClassController@index')->name('view.res.classes');
Route::delete('/delete/res-class-mgmt/{id}', 'ResidentialClassController@deleteResClass')->name('res.class.delete');
Route::get('/update/res-class-mgmt/{id}', 'ResidentialClassController@updateResClassForm')->name('res.class.update.form');
Route::post('/update/res-class-mgmt/{id}', 'ResidentialClassController@updatedResClass')->name('res.class.edit');
Route::post('/add/res-class-mgmt', 'ResidentialClassController@addResClass')->name('res.class.store');
Route::get('/res-class-mgmt/form', 'ResidentialClassController@showResClassForm')->name('res.class.store.form');

//Rating Locality Routes
Route::get('/app-settings/locality-mgmt', 'LocalityController@index')->name('view.localities');
Route::delete('/delete/locality-mgmt/{id}', 'LocalityController@deleteLocality')->name('locality.delete');
Route::get('/update/locality-mgmt/{id}', 'LocalityController@updateLocalityForm')->name('locality.update.form');
Route::post('/update/locality-mgmt/{id}', 'LocalityController@updatedLocality')->name('locality.edit');
Route::post('/add/locality-mgmt', 'LocalityController@addLocality')->name('locality.store');
Route::get('/locality-mgmt/form', 'LocalityController@showLocalityForm')->name('locality.store.form');

//Main Rating Categories Routes
Route::get('/app-settings/main/categories/fee-mgmt', 'MainRateCategoryController@index')->name('view.main.category');
Route::delete('/main/categories/fee-mgmt/delete/{id}', 'MainRateCategoryController@deleteMainCategory')->name('main.category.delete');
Route::get('/main/categories/update/form/fee-mgmt/{id}', 'MainRateCategoryController@updateMainCategoryForm')->name('main.category.update.form');
Route::put('/main/categories/update/fee-mgmt/{id}', 'MainRateCategoryController@updatedMainCategory')->name('main.category.edit');
Route::post('/main/categories/create/fee-mgmt', 'MainRateCategoryController@create')->name('main.category.store');
Route::get('/main/categories/fee-mgmt/form', 'MainRateCategoryController@createForm')->name('main.category.store.form');

//Sub Rating Categories Routes
Route::get('/app-settings/sub/categories/fee-mgmt', 'SubRateCategoryController@index')->name('view.sub.category');
Route::delete('/sub/categories/fee-mgmt/delete/{id}', 'SubRateCategoryController@delete')->name('sub.category.delete');
Route::get('/sub/categories/update/form/fee-mgmt/{id}', 'SubRateCategoryController@updateForm')->name('sub.category.update.form');
Route::put('/sub/categories/update/fee-mgmt/{id}', 'SubRateCategoryController@updated')->name('sub.category.edit');
Route::post('/sub/categories/create/fee-mgmt', 'SubRateCategoryController@create')->name('sub.category.store');
Route::get('/sub/categories/fee-mgmt/form', 'SubRateCategoryController@createForm')->name('sub.category.store.form');

//Fee fixing Routes
Route::get('/app-settings/fees/fixing/fee-mgmt', 'FeeFixingController@index')->name('view.fees.fixing');
Route::delete('/fees/fixing/fee-mgmt/delete/{id}', 'FeeFixingController@delete')->name('fees.fixing.delete');
Route::get('/fees/fixing/update/form/fee-mgmt/{id}', 'FeeFixingController@updateForm')->name('fees.fixing.update.form');
Route::put('/fees/fixing/update/fee-mgmt/{id}', 'FeeFixingController@updated')->name('fees.fixing.edit');
Route::post('/fees/fixing/create/fee-mgmt', 'FeeFixingController@create')->name('fees.fixing.store');
Route::get('/fees/fixing/fee-mgmt/form', 'FeeFixingController@createForm')->name('fees.fixing.store.form');