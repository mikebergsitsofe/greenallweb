<?php

use App\Http\Controllers\UUIDController;
use App\Property;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('v1/properties/', function () {
    $props= Property::all();
    $count=count($props);
    if($count<1){
        $array = array(
            'status' => 500,
            'message' => 'No Record Found'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }elseif ($count>0){
        $array = array(
            'status' => 200,
            'properties' => $props->toArray()
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 404,
            'message' => 'Sorry! No Resource Found Matching the URL'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }

    return $res;
});

Route::get('v1/properties/{uuid}', function ($uuid) {
    $property= DB::table('property')->where('uuid', $uuid)->get();
    $count=count($property);
    if($count<1){
        $array = array(
            'status' => 500,
            'message' => 'No Record Found'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }elseif ($count>0){
        $array = array(
            'status' => 200,
            'property' => $property->toArray()
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 200,
            'property' => 'Sorry! No Resource Found Matching the URL'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }
    return $res;
});

//Submit data to db Routes
Route::post('v1/add/properties', function (Request $request) {
    $data = $request->json()->all();
    $myup=DB::table('properties')
        ->insert($data);
    if(!$myup){
        $array = array(
            'status' => 500,
            'message' => 'Ooops! Record not Added'

        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }elseif ($myup){
        $array = array(
            'status' => 200,
            'message' => 'Property Successfully Added'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 200,
            'property' => 'Sorry! No Resource Found Matching the URL'

        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }
    return $res;
});

Route::post('v1/add/bops', function (Request $request) {
    $data = $request->json()->all();
    $myup=DB::table('bops')
        ->insert($data);
    if(!$myup){
        $array = array(
            'status' => 500,
            'message' => 'Ooops! Record not Added'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }elseif ($myup){
        $array = array(
            'status' => 200,
            'message' => 'Business Successfully Registered'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 200,
            'message' => 'Sorry! No Resource Found Matching the URL'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }
    return $res;
});

Route::post('v1/add/markets', function (Request $request) {
    $data = $request->json()->all();
    $myup=DB::table('markets')
        ->insert($data);
    if(!$myup){
        $array = array(
            'status' => 500,
            'message' => 'Ooops! Record not Added'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }elseif ($myup){
        $array = array(
            'status' => 200,
            'message' => 'Successfully Registered'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 200,
            'message' => 'Sorry! No Resource Found Matching the URL'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }
    return $res;
});

//Submit data to db Routes
Route::post('v1/add/observations', function (Request $request) {
    $data = $request->json()->all();
    $uuid=UUIDController::v4();
    $fields = json_encode($data, true);
    $r=json_decode($fields);
    //$m=$r.','.'uuid:'.$uuid;
    return $r->name; //json_encode($m,true) ; //UUIDController::v4();

});

Route::post('v1/user/login', function (Request $request) {
    $data = $request->json()->all();
    $email=$data['email'];
    $pw=$data['password'];
    $user=DB::table('users')->where('email',$email)->get();
    $myUser=$user->toArray();

    $count=count($user);
    if($count<1) {
        $array = array(
            'status' => 500,
            'message' => 'Check Email'
        );
        $res = json_encode($array, JSON_PRETTY_PRINT);
    }elseif (Hash::check($pw, $myUser['0']->password))
    {
        $array = array(
            'status' => 200,
            'id' => $myUser['0']->id,
            'fullname' => $myUser['0']->firstname." ".$myUser['0']->lastname,
            'email' => $myUser['0']->email,
                'assembly'=>DB::table('assemblies')->where('id',$myUser['0']->assembly_id)->select('id','name')->get(),
                'sub-metro'=>DB::table('submetros')->where('id',$myUser['0']->submetro_id)->select('id','name')->get(),

            'phone1' => $myUser['0']->phone1,
            'token' => $myUser['0']->auth_token
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }else{
        $array = array(
            'status' => 500,
            'message' => 'Check Password'
        );
        $res=json_encode($array, JSON_PRETTY_PRINT);
    }

    return $res;
});



