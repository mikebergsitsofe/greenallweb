@extends('user-mgmt.base')
@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <div class="row" >
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Register New User</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <span>Bio-Data</span>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">First Name<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="lastname" class="col-md-4 control-label">Last Name<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('othername') ? ' has-error' : '' }}">
                                <label for="othername" class="col-md-4 control-label">Other Name</label>

                                <div class="col-md-6">
                                    <input id="othername" type="text" class="form-control" name="othername" value="{{ old('othername') }}">

                                    @if ($errors->has('othername'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('othername') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">gender<span>*</span></label>

                                <div class="col-md-6">
                                    <select id="gender"  name="gender" class="form-control" required>
                                        <option value="" disabled selected>Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">other</option>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="avatar" class="col-md-4 control-label">Picture</label>
                                <div class="col-md-6">

                                    <input type="file" name="user_image" accept="image/*">
                                </div>
                            </div>
                            <hr><span>Jurisdiction</span>
                            <div class="form-group{{ $errors->has('phone1') ? ' has-error' : '' }}">
                                <label for="phone1" class="col-md-4 control-label">Phone 1<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="phone1" type="tel" class="form-control" name="phone1" value="{{ old('phone1') }}" required>

                                    @if ($errors->has('phone1'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone1') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone2') ? ' has-error' : '' }}">
                                <label for="phone2" class="col-md-4 control-label">Phone 2</label>

                                <div class="col-md-6">
                                    <input id="phone2" type="tel" class="form-control" name="phone2" value="{{ old('phone2') }}">

                                    @if ($errors->has('phone2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('assembly') ? ' has-error' : '' }}">
                                <label for="assembly" class="col-md-4 control-label">Assembly<span>*</span></label>

                                <div class="col-md-6">
                                    <select id="assembly"  name="assembly" class="form-control" required>
                                        <option value="" disabled selected>Select the Assembly of this User</option>
                                        @foreach($assemblies as $assembly)
                                            <option value="{{$assembly->id}}">{{$assembly->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('assembly'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('assembly') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('submetro') ? ' has-error' : '' }}">
                                <label for="submetro" class="col-md-4 control-label">Sub-Metro<span>*</span></label>

                                <div class="col-md-6">
                                    <select id="submetro"  name="submetro" class="form-control" required>
                                        <option value="" disabled selected>Select Sub-Metro of this User</option>
                                        @foreach($submetros as $submetro)
                                            <option value="{{$submetro->id}}">{{$submetro->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('submetro'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('submetro') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <hr><span>Security</span>
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">User Name<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password<span>*</span></label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection