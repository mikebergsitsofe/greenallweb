@extends('user-mgmt.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">List of System Users</h3>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-primary" href="{{route('user.store.form')}}">Add New System User</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
        </div>
        <?php
        $count=1;
        ?>

        <div class="box-body">
            @include('modals.delete_collected')
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="table table-striped table-hover category-table" id="dataTables-example" data-toggle="dataTable" data-form="deleteForm">
                        <thead>
                        <tr>
                            <th>SN#</th>
                            <th>UserID</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td> {{$count++}}</td>
                                <td> {{$user->uuid}}</td>
                                <td>{{$user->firstname." ".$user->lastname." ".$user->othername}}</td>
                                <td> {{$user->gender}}</td>
                                <td>
                                    <div class="row">
                                        <!--Edit Column-->
                                        <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                            <a href="{{route('user.update.form', $user->id)}}" class="btn btn-info btn-md"><i class=" fa fa-edit"></i></a>
                                        </div>
                                        <!--Delete Column-->
                                        <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                            {!! Form::model($user, ['method' => 'delete', 'route' => ['user.delete', $user->id], 'class' =>'form-inline form-delete']) !!}
                                            {!! Form::hidden('id', $user->id) !!}
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class' => 'btn btn-md btn-danger delete', 'name' => 'delete_modal']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading bg-yellow-gradient">About MMDAs in Ghana</div>
                        <div class="panel-body">
                            <p>About MMDAs of Ghana</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </section>
    <!-- /.content -->

@endsection

@section('mask-scripts')
    <script src="{{ asset ("/mask_js/confirm-delete.js") }}" type="text/javascript"></script>
    <script src="{{ asset('datatables/js/datatable-normal.js') }}"></script>
@endSection

