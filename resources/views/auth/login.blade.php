@extends('layouts.clear')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 100px;">
        <div class="col-md-4 col-md-offset-4 text-center">
            <a  href="#" style="color:white;"><b style="color:white; font-size: xx-large;">Greenfield</b><br><span style="color:white; font-size: large;" >Management Ltd.</span></a>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #01361C; color: #fff;">
                    <img src="{{asset('images/gf-logo.png')}}" width="50px" height="50px"> <span class="panel-heading">Login</span>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label"><i class="fa fa-envelope" style="font-size: large;"></i></label>

                            <div class="col-md-8">
                                <input id="email" placeholder="Email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 control-label"><i class="fa fa-lock" style="font-size: x-large;"></i></label>

                            <div class="col-md-8">
                                <input id="password" placeholder="Password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="panel-footer text-center" >
                <a href="http://bitlogictechnologies.com"><h4> CoreDev:copyright&copy;2017</h4></a>
            </div>
        </div>
    </div>

</div>
@endsection
