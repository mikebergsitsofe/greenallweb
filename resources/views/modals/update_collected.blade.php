<div class="box">
    <div class="modal" id="updateDialog">
        <div class="modal-dialog">
           <div class="panel panel-primary">
               <div class="panel-heading">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                   <img src="{{asset('images/gf-logo.png')}}" width="30px" height="30px">
                   <span class="modal-title">Collected Revenue Record Update Dialog</span>
               </div>
               <div class="panel-body">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group{{ $errors->has('date-received') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="glyphicon glyphicon-time"></i></span>
                                    <input type="date" id="date_received" name="date_received"  class="form-control" placeholder="Date Received" required  maxlength="10">
                                    <br>

                                </div>
                                @if ($errors->has('date-received'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('date-received') }}</strong>
                                </span>
                                @endif
                                <br>

                                <div class="input-group{{ $errors->has('account_no') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="fa fa-building"></i></span>
                                    <input type="text" id="account_no" name="account_no" class="form-control" placeholder="Account Number (Last 8 Digits e.d 15117000)" required autofocus>
                                    <br>
                                </div>
                                <span class="help-block">
                                    <a class="btn btn-primary" onclick="al()" href="#" style="width:100%;"><i class="fa fa-search-plus"></i>Search</a>
                                </span>
                                @if ($errors->has('account_no'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('account_no') }}</strong>
                                </span>
                                @endif
                                <br>
                                <div class="input-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" id="account_name"  name="account_name" class="form-control" placeholder="Account Name" required>
                                    <br>

                                </div>
                                @if ($errors->has('account_name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('account_name') }}</strong>
                                </span>
                                @endif
                                <br>
                                <div class="input-group{{ $errors->has('receipt_no') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="glyphicon glyphicon-record"></i></span>
                                    <input type="text" id="receipt_no"  name="receipt_no" class="form-control" list="prefix" placeholder="Receipt Number " required>
                                    <datalist id="prefix">
                                        <option>14/00</option>
                                    </datalist>
                                    <br>

                                </div>
                                @if ($errors->has('receipt_no'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('receipt_no') }}</strong>
                                </span>
                                @endif
                                <br>

                                <div class="input-group{{ $errors->has('current') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="glyphicon glyphicon-stats"></i></span>
                                    <input type="text" id="current"  name="current" class="form-control" list="states" placeholder="Current">
                                    <datalist id="states">
                                        <option>Current</option>
                                    </datalist>
                                    <br>

                                </div>
                                @if ($errors->has('current'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('current') }}</strong>
                                </span>
                                @endif
                                <br>

                            </div>

                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('arrears') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="glyphicon glyphicon-stats"></i></span>
                                    <input type="text" id="arrears" name="arrears" class="form-control" list="defs" placeholder="Arrears">
                                    <datalist id="defs">
                                        <option>Arrears</option>
                                    </datalist>
                                    <br>

                                </div>
                                @if ($errors->has('arrears'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('arrears') }}</strong>
                                </span>
                                @endif
                                <br>
                                <div class="input-group{{ $errors->has('pay_method') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="fa fa-money"></i></span>
                                    <select id="pay_method" onchange="showBank()"  name="pay_method" class="form-control" required>
                                        <option value="" disabled selected>Select a Payment Method</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Mobile Money">Mobile Money</option>
                                        <option value="Bank Transfer">Bank Transfer</option>
                                    </select>
                                    <br>

                                </div>
                                @if ($errors->has('pay_method'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('pay_method') }}</strong>
                                </span>
                                @endif
                                <br>
                                <div id="ifCheque" style="display:block">
                                    <div class="input-group{{ $errors->has('cheque_no') ? ' has-error' : '' }}">
                                        <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="fa fa-check-circle"></i></span>
                                        <input type="text" id="cheque_no"   name="cheque_no" class="form-control" placeholder="Cheque Number " >
                                        <br>

                                    </div>
                                    @if ($errors->has('cheque_no'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('cheque_no') }}</strong>
                                </span>
                                    @endif
                                    <br>
                                    <div class="input-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                                        <span class="input-group-addon" style="background-color:#e6f7ff;"><i class="fa fa-bank"></i></span>
                                        <input type="text" id="bank" name="bank" class="form-control" list="banks" placeholder="bank" >
                                        <datalist id="banks">
                                            <option>Ecobank</option>
                                            <option>GCB</option>
                                            <option>UniBank</option>
                                            <option>Prudential</option>
                                        </datalist>
                                        <br>

                                    </div>
                                    @if ($errors->has('bank'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('bank') }}</strong>
                                </span>
                                    @endif

                                </div>
                                <br>

                                <div class="input-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><strong class="">GH¢</strong></span>
                                    <input type="text" id="amount" name="amount" class="form-control" placeholder="Amount in GH¢" required>
                                    <br>

                                </div>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif
                                <br>
                                <div class="input-group{{ $errors->has('search_key') ? ' has-error' : '' }}">
                                    <span class="input-group-addon" style="background-color:#e6f7ff;"><strong class="fa fa-key"></strong></span>
                                    <input type="text" id="search_key" name="search_key" class="form-control" value="{{ old('search_key') }}" placeholder="Enter Unique ID for this Transaction" required>
                                    <br>

                                </div>
                                @if ($errors->has('search_key'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('search_key') }}</strong>
                                </span>
                                @endif
                                <br>
                            </div>

                        </div>
                    </form>
               </div>

               <div id="parent" class="panel-footer">
                   <button type="submit" class="btn btn-sm btn-success block center" id="update-btn">Update</button>
                   <button type="button" class="btn btn-sm btn-default block center" data-dismiss="modal">Cancel</button>
               </div>

           </div>
        </div>
    </div>
    <script>
        function showBank() {
            alert('nnvks');
            if (document.getElementById('pay_method').value === 'Cheque') {
                document.getElementById('ifCheque').style.display = 'block';
            }
            else
                document.getElementById('ifCheque').style.display = 'none';
        }

        $(document).on("click", ".open-UpdateCollectedDialog", function () {
            var myBookId = $(this).data('id');
            var myBookName = $(this).data('name');
            $("#account_no").val( myBookId);
            $(".modal-dialog #account_name").val( myBookName);
        });


    </script>
</div>