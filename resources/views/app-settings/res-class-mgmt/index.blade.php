@extends('app-settings.res-class-mgmt.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @if (Session::has('message'))
                            <div class="alert alert-info alert-dismissible ">
                                <button type = "button" class="close" data-dismiss = "alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="box-title">List of Residential Classes</h3>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{route('res.class.store.form')}}">Add new Residential Class</a>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{route('locality.store.form')}}">Add new Locality</a>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{route('locality.store.form')}}">Add new Assembly</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
        </div>
        <?php
        $count=1;
        ?>

            <div class="box-body">
                @include('modals.delete_collected')
                <div class="row">
                    <div class="col-md-8">
                        <table width="100%" class="table table-striped table-hover " id="dataTables-example" data-toggle="dataTable" data-form="deleteForm">
                            <thead>
                            <tr>
                                <th>SN#</th>
                                <th>ID#</th>
                                <th>Description</th>
                                <th>Rate Amount</th>
                                <th>Rating Year</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($residential_classes as $residential_class)
                                <tr>
                                    <td> {{$count++}}</td>
                                    <td> {{$residential_class->uuid}}</td>
                                    <td> {{$residential_class->description}}</td>
                                    <td>{{$residential_class->rate}}</td>
                                    <td> {{$residential_class->rating_year}}</td>
                                    <td>
                                        <div class="row">
                                            <!--Edit Column-->
                                            <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                                <a href="{{route('res.class.update.form', $residential_class->id)}}" class="btn btn-info btn-md"><i class=" fa fa-edit"></i></a>
                                            </div>
                                            <!--Delete Column-->
                                            <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                                {{ csrf_field() }}
                                                {!! Form::model($residential_class, ['method' => 'delete', 'route' => ['res.class.delete', $residential_class->id], 'class' =>'form-inline form-delete']) !!}
                                                {!! Form::hidden('id', $residential_class->id) !!}
                                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class' => 'btn btn-md btn-danger delete', 'name' => 'delete_modal']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading bg-yellow-gradient">About Setting Up Residential Classes</div>
                            <div class="panel-body">
                                <p>Residential Classes</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
    </section>
    <!-- /.content -->

@endsection

@section('mask-scripts')
    <script src="{{ asset ("/mask_js/confirm-delete.js") }}" type="text/javascript"></script>
    <script src="{{ asset('datatables/js/datatable-normal.js') }}"></script>
@endSection

