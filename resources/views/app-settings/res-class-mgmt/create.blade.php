@extends('app-settings.res-class-mgmt.base')

@section('action-content')
    <div class="container">
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Add new Residential Class</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('res.class.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Residential Class Name</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('rating_year') ? ' has-error' : '' }}">
                                <label for="rating_year" class="col-md-4 control-label">Rating Year</label>

                                <div class="col-md-6">
                                    <select id="rating_year"  name="rating_year" class="form-control" required>
                                        <option value="" disabled selected>Select the Rating Year of this Class</option>
                                        <option value="{{date("Y")}}">{{date("Y")}}</option>
                                        <option value="{{date("Y")+1}}">{{date("Y")+1}}</option>
                                    </select>
                                    @if ($errors->has('rating_year'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('rating_year') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('assembly') ? ' has-error' : '' }}">
                                <label for="assembly" class="col-md-4 control-label">Assembly</label>

                                <div class="col-md-6">
                                    <select id="assembly"  name="assembly" class="form-control" required>
                                        <option value="" disabled selected>Select the Assembly of this Class</option>
                                        @foreach($assemblies as $assembly)
                                        <option value="{{$assembly->id}}">{{$assembly->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('assembly'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('assembly') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                                <label for="rate" class="col-md-4 control-label">Approved Rate</label>

                                <div class="col-md-6">
                                    <input id="rate" type="text" class="form-control" name="rate" value="{{ old('rate') }}" required>
                                    @if ($errors->has('rate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('rate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
