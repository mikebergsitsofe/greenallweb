@extends('app-settings.locality-mgmt.base')

@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Add new Locality Class</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('locality.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Locality Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('residential_class_id') ? ' has-error' : '' }}">
                                <label for="residential_class_id" class="col-md-4 control-label">Residential Class</label>

                                <div class="col-md-6">
                                    <select id="residential_class_id"  name="residential_class_id" class="form-control" required>
                                        <option value="" disabled selected>Select the Residential Class of this Locality</option>
                                        @foreach($residential_classes as $residential_class)
                                        <option value="{{$residential_class->id}}">{{$residential_class->description}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('residential_class_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('residential_class_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
