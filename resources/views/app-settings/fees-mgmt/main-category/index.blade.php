@extends('app-settings.fees-mgmt.main-category.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @if (Session::has('message'))
                            <div class="alert alert-info alert-dismissible ">
                                <button type = "button" class="close" data-dismiss = "alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">List of Fee Fixing Categories</h3>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-primary" href="{{route('main.category.store.form')}}">Add new Main Category</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
        </div>
        <?php
        $count=1;
        ?>

            <div class="box-body">
                @include('modals.delete_collected')
                <div class="row">
                    <div class="col-md-6">
                        <table width="100%" class="table table-striped table-hover category-table" id="dataTables-example" data-toggle="dataTable" data-form="deleteForm">
                            <thead>
                            <tr>
                                <th>SN#</th>
                                <th>Main Category</th>
                                <th>Assembly</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($main_categories as $main_category)
                                <tr>
                                    <td> {{$count++}}</td>
                                    <td> {{$main_category->category}}</td>
                                    <td>{{$main_category->assembly}}</td>
                                    <td>
                                        <div class="row">
                                            <!--Edit Column-->
                                            <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                                <a href="{{route('locality.update.form', $main_category->id)}}" class="btn btn-info btn-md"><i class=" fa fa-edit"></i></a>
                                            </div>
                                            <!--Delete Column-->
                                            <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                                {!! Form::model($main_category, ['method' => 'delete', 'route' => ['locality.delete', $main_category->id], 'class' =>'form-inline form-delete']) !!}
                                                {!! Form::hidden('id', $main_category->id) !!}
                                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class' => 'btn btn-md btn-danger delete', 'name' => 'delete_modal']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                                <th style="border-top: none;"></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading bg-yellow-gradient">About Setting Up Fixed Fees</div>
                            <div class="panel-body">
                                <p>Setting Up Fixed Fees</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
    </section>
    <!-- /.content -->

@endsection

@section('mask-scripts')
    <script src="{{ asset ("/mask_js/confirm-delete.js") }}" type="text/javascript"></script>
    <script src="{{ asset('datatables/js/datatable-normal.js') }}"></script>
@endSection

