@extends('app-settings.fees-mgmt.sub-category.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @if (Session::has('message'))
                            <div class="alert alert-info alert-dismissible ">
                                <button type = "button" class="close" data-dismiss = "alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="box-title">List of Fee Fixing Sub Categories</h3>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-primary" href="{{route('sub.category.store.form')}}">Add new Sub Category</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
        </div>
        <?php
        $count=1;
        ?>

        <div class="box-body">
            @include('modals.delete_collected')
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="table table-striped table-hover category-table" id="dataTables-example" data-toggle="dataTable" data-form="deleteForm">
                        <thead>
                        <tr>
                            <th>SN#</th>
                            <th>Sub Category</th>
                            <th>Main Category</th>
                            <th>Assembly</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sub_categories as $sub_category)
                            <tr>
                                <td> {{$count++}}</td>
                                <td> {{$sub_category->sub_category}}</td>
                                <td>{{$sub_category->main_category}}</td>
                                <td>{{$sub_category->assembly}}</td>
                                <td>
                                    <div class="row">
                                        <!--Edit Column-->
                                        <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                            <a href="{{route('locality.update.form', $sub_category->id)}}" class="btn btn-info btn-md"><i class=" fa fa-edit"></i></a>
                                        </div>
                                        <!--Delete Column-->
                                        <div class="col-xs-6" style="padding-left: 0;padding-right: 0;">
                                            {!! Form::model($sub_category, ['method' => 'delete', 'route' => ['locality.delete', $sub_category->id], 'class' =>'form-inline form-delete']) !!}
                                            {!! Form::hidden('id', $sub_category->id) !!}
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class' => 'btn btn-md btn-danger delete', 'name' => 'delete_modal']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                            <th style="border-top: none;"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading bg-yellow-gradient">About Setting Up Fixed Fees</div>
                        <div class="panel-body">
                            <p>Setting Up Fixed Fees</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </section>
    <!-- /.content -->

@endsection

@section('mask-scripts')
    <script src="{{ asset ("/mask_js/confirm-delete.js") }}" type="text/javascript"></script>
    <script src="{{ asset('datatables/js/datatable-normal.js') }}"></script>
@endSection

