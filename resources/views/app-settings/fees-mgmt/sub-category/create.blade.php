@extends('app-settings.fees-mgmt.sub-category.base')

@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Define new Sub Fee Category</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('sub.category.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('sub_category_name') ? ' has-error' : '' }}">
                                <label for="sub_category_name" class="col-md-4 control-label">Sub Category Name</label>

                                <div class="col-md-6">
                                    <input id="sub_category_name" type="text" class="form-control" name="sub_category_name" value="{{ old('sub_category_name') }}" required autofocus>

                                    @if ($errors->has('sub_category_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sub_category_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('main_rate_category_id') ? ' has-error' : '' }}">
                                <label for="main_rate_category_id" class="col-md-4 control-label">Main Category Name</label>

                                <div class="col-md-6">
                                    <select id="main_rate_category_id"  name="main_rate_category_id" class="form-control" required>
                                        <option value="" disabled selected>Select Main Category</option>
                                        @foreach($main_categories as $main_category)
                                            <option value="{{$main_category->id}}">{{$main_category->main_category_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('main_rate_category_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('main_rate_category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
