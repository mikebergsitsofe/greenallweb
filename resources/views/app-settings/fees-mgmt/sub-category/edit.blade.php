@extends('app-settings.fees-mgmt.sub-category.base')

@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Edit Main Fee Category</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('locality.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('main_category_name') ? ' has-error' : '' }}">
                                <label for="main_category_name" class="col-md-4 control-label">Main Fee Fixing Category Name</label>

                                <div class="col-md-6">
                                    <input id="main_category_name" type="text" class="form-control" name="main_category_name" value="{{ old('main_category_name') }}" required autofocus>

                                    @if ($errors->has('main_category_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('main_category_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
