@extends('app-settings.fees-mgmt.fee-fixing.base')

@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Define new Fix Fee </h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('fees.fixing.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Fee Name</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sub_rate_category_id') ? ' has-error' : '' }}">
                                <label for="sub_rate_category_id" class="col-md-4 control-label">Sub Category</label>

                                <div class="col-md-6">
                                    <select id="sub_rate_category_id"  name="sub_rate_category_id" class="form-control" required>
                                        <option value="" disabled selected>Select Sub Category</option>
                                        @foreach($sub_categories as $sub_category)
                                            <option value="{{$sub_category->id}}">{{$sub_category->sub_category_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('sub_rate_category_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sub_rate_category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('rating_year') ? ' has-error' : '' }}">
                                <label for="rating_year" class="col-md-4 control-label">Levy for the Year</label>

                                <div class="col-md-6">
                                    <select id="rating_year"  name="rating_year" class="form-control" required>
                                        <option value="" disabled selected>Select the Rating Year of this Fee</option>
                                        <option value="{{date("Y")}}">{{date("Y")}}</option>
                                        <option value="{{date("Y")+1}}">{{date("Y")+1}}</option>
                                    </select>
                                    @if ($errors->has('rating_year'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('rating_year') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                                <label for="rate" class="col-md-4 control-label">Levy Amount</label>

                                <div class="col-md-6">
                                    <input id="rate" type="number" class="form-control" name="rate" value="{{ old('rate') }}" required autofocus>

                                    @if ($errors->has('rate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('rate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
