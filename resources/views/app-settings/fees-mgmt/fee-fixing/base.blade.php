@extends('layouts.app-template')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Fee Fixing
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>App Settings</a></li>
                <li class="active">Fee Fixing Class</li>
            </ol>
        </section>
    @yield('action-content')
    <!-- /.content -->
    </div>

@endsection
