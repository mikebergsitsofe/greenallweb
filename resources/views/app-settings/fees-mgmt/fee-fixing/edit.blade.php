@extends('app-settings.fees-mgmt.fee-fixing.base')
@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <?php date("Y"); ?>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Update a Fixed Fee</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('locality.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('fee_name') ? ' has-error' : '' }}">
                                <label for="fee_name" class="col-md-4 control-label">Fee Name</label>

                                <div class="col-md-6">
                                    <input id="fee_name" type="text" class="form-control" name="fee_name" value="{{ old('fee_name') }}" required autofocus>

                                    @if ($errors->has('fee_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fee_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fee_amount') ? ' has-error' : '' }}">
                                <label for="fee_amount" class="col-md-4 control-label">Fee Name</label>

                                <div class="col-md-6">
                                    <input id="fee_amount" type="text" class="form-control" name="fee_amount" value="{{ old('fee_amount') }}" required autofocus>

                                    @if ($errors->has('fee_amount'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fee_amount') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
