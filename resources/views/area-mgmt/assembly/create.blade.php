@extends('area-mgmt.assembly.base')

@section('action-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #1f1f1f; color: white;"><h4>Add new Assembly</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('assembly.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Assembly Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('region') ? ' has-error' : '' }}">
                                <label for="region" class="col-md-4 control-label">Region</label>

                                <div class="col-md-6">
                                    <select id="region"  name="region" class="form-control" required>
                                        <option value="" disabled selected>Select the Region of this Assembly</option>
                                        @foreach($regions as $region)
                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('region'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('region') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('prefix_code') ? ' has-error' : '' }}">
                                <label for="prefix_code" class="col-md-4 control-label">Prefix Code</label>

                                <div class="col-md-6">
                                    <input id="prefix_code" type="text" class="form-control" name="prefix_code" value="{{ old('prefix_code') }}" required>
                                    @if ($errors->has('prefix_code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('prefix_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
