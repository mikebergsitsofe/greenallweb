@extends('layouts.app-template')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sub-Metro Mangement
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Area Management</a></li>
                <li class="active">Sub-Metro</li>
            </ol>
        </section>
    @yield('action-content')
    <!-- /.content -->
    </div>

@endsection
