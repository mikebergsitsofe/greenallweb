@extends('area-mgmt.submetro.base')

@section('action-content')
    <div class="container">
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Sub-Metro</div>
                    <div class="panel-body">
                        {!! Form::model($submetros, ['method' => 'PUT', 'route' => ['submetro.edit', $submetros['id']], 'class' =>'form-horizontal']) !!}
                            {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Sub-Metro Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$submetros['submetro_name']}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('assembly') ? ' has-error' : '' }}">
                            <label for="assembly" class="col-md-4 control-label">Assembly</label>

                            <div class="col-md-6">
                                <select id="assembly"  name="assembly" class="form-control" required>
                                    <option value="{{$submetros['assembly_id']}}" selected>{{$submetros['assembly_name']}}</option>
                                    @foreach($assemblies as $assembly)
                                        <option value="{{$assembly->id}}">{{$assembly->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('assembly'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('assembly') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('prefix_code') ? ' has-error' : '' }}">
                            <label for="prefix_code" class="col-md-4 control-label">Prefix Code</label>

                            <div class="col-md-6">
                                <input id="prefix_code" type="text" class="form-control" name="prefix_code" value="{{ $submetros['prefix'] }}" required>
                                @if ($errors->has('prefix_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prefix_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
