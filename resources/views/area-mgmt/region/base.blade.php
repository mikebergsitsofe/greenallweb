@extends('layouts.app-template')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Region Mangement
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Area Management</a></li>
                <li class="active">Region</li>
            </ol>
        </section>
    @yield('action-content')
    <!-- /.content -->
    </div>

@endsection
@section('script')
    <script src="{{ asset('datatables/js/table-without-sum.js') }}"></script>
@endsection
