  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs" style="color: yellowgreen; font-weight: bolder;">
      <marquee>--Achieve More--</marquee>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#" style="color: #1b4b33;">Greenfied Management Ltd.</a>.</strong> All rights reserved.
  </footer>