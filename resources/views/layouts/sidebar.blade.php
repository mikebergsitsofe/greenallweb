  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar " >

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar ">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel" >
        <div class="pull-left image">
          <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="/"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Property Rate Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Register Property</a></li>
            <li><a href="#">Search</a></li>
            <li><a href="#">Report</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Area Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('view.regions')}}">Region</a></li>
            <li><a href="{{route('view.assemblies')}}">Assembly</a></li>
            <li><a href="{{route('view.submetros')}}">Sub-Metro</a></li>
            <li><a href="#">Report</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>System User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('view.users')}}">Register New User</a></li>
            <li><a href="#">Report</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Application System Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="margin-left: 20px;">
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Residential Class Rates</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('view.res.classes')}}">New Add Residential Class</a></li>
                <li><a href="{{route('view.localities')}}">New Add Locality</a></li>
                <li><a href="#">Report</a></li>
              </ul>
            </li>
          </ul>
          <ul class="treeview-menu" style="margin-left: 20px;">
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Fee Fixing</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('view.main.category')}}">New Main Fee Category</a></li>
                <li><a href="{{route('view.sub.category')}}">New Sub Fee Category</a></li>
                <li><a href="{{route('view.fees.fixing')}}">Fix New Fee</a></li>
                <li><a href="#">Report</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><a href="{{asset('apidocs')}}">Api Docs</a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>