<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('geo_lng', 60)->nullable();
            $table->string('geo_lat', 60)->nullable();
            $table->string('accuracy',10)->nullable();
            $table->string('name', 60)->nullable();
            $table->string('director_name', 60)->nullable();
            $table->string('bus_reg_no', 60)->nullable();
            $table->string('phone1', 15)->nullable();
            $table->string('phone2', 15)->nullable();
            $table->string('address', 30)->nullable();
            $table->string('type_of_business', 100)->nullable();
            $table->string('tin_no', 60)->nullable();
            $table->string('nearest_landmark', 60)->nullable();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bops');
    }
}
