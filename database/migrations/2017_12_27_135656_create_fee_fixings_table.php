<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeFixingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_fixings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('description', 200)->nullable();
            $table->string('rating_year', 10)->nullable();
            $table->string('rate', 20)->nullable();
            $table->integer('sub_rate_category_id')->unsigned()->index();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_fixings');
    }
}
