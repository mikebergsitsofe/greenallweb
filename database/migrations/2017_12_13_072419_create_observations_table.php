<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('geo_lng', 60)->nullable();
            $table->string('geo_lat', 60)->nullable();
            $table->string('accuracy',10)->nullable();
            $table->string('name', 60)->nullable();
            $table->string('respondent_type', 60)->nullable();
            $table->string('cust_no', 20)->nullable();
            $table->string('phone', 13)->nullable();
            $table->string('amount_paid', 20)->nullable();
            $table->string('receipt_no', 20)->nullable();
            $table->text('receipt_image')->nullable();
            $table->string('paid_date', 10)->nullable();
            $table->string('remarks', 255)->nullable();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->softDeletes();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');

            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}
