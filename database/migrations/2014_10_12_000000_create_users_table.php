<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid',20);
            $table->string('lastname',60);
            $table->string('firstname',60);
            $table->string('othername',60)->nullable();
            $table->string('email',100)->unique();
            $table->string('username',50)->unique();
            $table->string('password');
            $table->string('gender',10);
            $table->string('phone1',13);
            $table->string('phone2',13)->nullable();
            $table->text('image')->nullable();
            $table->text('auth_token')->nullable();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();
            $table->rememberToken();
            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
