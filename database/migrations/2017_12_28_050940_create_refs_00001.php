<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefs00001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('assemblies', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('submetros', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('residential_classes', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');
            $table->foreign('residential_class_id')->references('id')->on('residential_classes');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('supervisors', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('agents', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('agent_supervisors', function (Blueprint $table) {
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->foreign('supervisor_id')->references('id')->on('supervisors');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
        Schema::table('localities', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');
            $table->foreign('residential_class_id')->references('id')->on('residential_classes');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('main_rate_categories', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('sub_rate_categories', function (Blueprint $table) {
            $table->foreign('main_rate_category_id')->references('id')->on('main_rate_categories');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');

            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');
        });

        Schema::table('fee_fixings', function (Blueprint $table) {
            $table->foreign('assembly_id')->references('id')->on('assemblies');
            $table->foreign('submetro_id')->references('id')->on('submetros');

            $table->foreign('sub_rate_category_id')->references('id')->on('sub_rate_categories');

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
