<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainRateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_rate_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('main_category_name', 200)->nullable();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_rate_categories');
    }
}
