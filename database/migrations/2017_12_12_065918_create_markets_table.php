<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('geo_lng', 60)->nullable();
            $table->string('geo_lat', 60)->nullable();
            $table->string('accuracy',10)->nullable();

            $table->string('lastname',60);
            $table->string('firstname',60);
            $table->string('othername',60)->nullable();

            $table->string('seller_type', 60)->nullable();
            $table->string('hawker_type', 60)->nullable();
            $table->string('gender', 60)->nullable();
            $table->string('phone1', 15)->nullable();
            $table->string('phone2', 15)->nullable();
            $table->text('image')->nullable();
            $table->string('id_type', 30)->nullable();
            $table->string('id_card_no', 30)->nullable();
            $table->string('mobile_network', 60)->nullable();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets');
    }
}
