<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('geo_lng', 60)->nullable();
            $table->string('geo_lat', 60)->nullable();
            $table->string('accuracy',10)->nullable();
            $table->string('property_type', 60)->nullable();
            $table->string('residential_id', 60)->nullable();
            $table->string('gender', 60)->nullable();
            $table->string('phone1', 15)->nullable();
            $table->string('phone2', 15)->nullable();
            $table->string('ownership_status', 30)->nullable();
            $table->string('registration_status', 30)->nullable();
            $table->string('electric_source', 60)->nullable();
            $table->string('no_of_units', 5)->nullable();
            $table->string('nearest_landmark', 60)->nullable();
            $table->text('image')->nullable();

            $table->integer('residential_class_id')->unsigned()->index();
            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->string('indenture_no', 10)->nullable();
            $table->string('locality', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
