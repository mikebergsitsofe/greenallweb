<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_rate_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 14);
            $table->string('sub_category_name', 200)->nullable();

            $table->integer('main_rate_category_id')->unsigned()->index();

            $table->integer('assembly_id')->unsigned()->index();
            $table->integer('submetro_id')->unsigned()->index();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->index();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_rate_categories');
    }
}
