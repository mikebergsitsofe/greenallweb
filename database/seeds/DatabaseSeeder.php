<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $user = factory(App\User::class)->create([
             'uuid' => 'aski0246102372',
             'email' => 'admin@gmail.com',
             'username' => 'systemadmin',
             'password' => bcrypt('123456'),
             'lastname' => 'System',
             'firstname' => 'Administrator',
             'gender' => 'male',
             'phone1' => '0246102372',
             'phone2' => '0246102372',
             'image' => 'avatars/MFkg5FW8R9UyQrWweSKAwGuWCZTZBYUXgtBHTAR7.jpeg',
             'auth_token' => 'fhjjkjhvcvkjxhjb6765456rtyjfhjgukbulupoihihggkjh dtyghjmjhdfghhjv',
             'assembly_id' => '1',
             'submetro_id' => '1',
             'created_by' => '1',
             'updated_by' => '1'
         ]);
    }
}
