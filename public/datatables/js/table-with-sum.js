$(document).ready(function() {
    $('#dataTables-example').DataTable({

        "order": [[ 0, "desc" ],[1,"desc"]],
        responsive: true,

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\GH¢,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over this page
            total = api
                .column(9)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 9 ).footer() ).html(
                'GH¢'+pageTotal +"<br><hr>"+' GH¢'+ total);

            $( api.column( 8 ).footer() ).html('Sub Total');

        }
    });

});
