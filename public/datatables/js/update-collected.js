$(document).ready(function() {
    var ur=window.location.href;
    var urSplit=ur.split('/');
    var id=urSplit[5];

    var date_received = document.getElementById('date_received');
    var account_no = document.getElementById('account_no');
    var account_name = document.getElementById('account_name');
    var receipt_no = document.getElementById("receipt_no");
    var current = document.getElementById("current");
    var arrears = document.getElementById("arrears");
    var bank = document.getElementById("bank");
    var cheque_no = document.getElementById("cheque_no");
    var search_key = document.getElementById("search_key");
    var amount = document.getElementById("amount");

    // process the form
    $('form').submit(function(event) {


        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            date_received   : date_received.val(),
            account_no      : account_no.val(),
            account_name    : account_name.val(),
            receipt_no      : receipt_no.val(),
            current         :current.val(),
            arrears         :arrears.val(),
            bank            :bank.val(),
            cheque_no       :cheque_no.val(),
            search_key      :search_key.val(),
            amount          :amount.val()

        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'edit/prop-payment/'+id, // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode          : true
        })
        // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
});