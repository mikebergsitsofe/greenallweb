<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    public function group()
    {
        return $this->belongsTo('residential_classes');
    }

    protected function getMylistABC() {

        $o_data = Locality::with('residential_classes')->get();
        print $o_data;

    }
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
