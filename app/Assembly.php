<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assembly extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assemblies';


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
