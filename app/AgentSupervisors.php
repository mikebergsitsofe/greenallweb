<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentSupervisors extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agent_supervisors';


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
