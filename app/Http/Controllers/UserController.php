<?php

namespace App\Http\Controllers;

use App\Assembly;
use App\SubMetro;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user-mgmt/index', ['users' => $users]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteUser()
    {
        //
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUser()
    {
        //
    }

    /**
     * Load Region Update Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserForm()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  addUser(Request $request){
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:60',
            'lastname' => 'required|max:60',
            'othername' => 'nullable|max:60',
            'gender' => 'required|max:6',
            'user_image'=>'required|image|mimes:jpg,png,jpeg|max:2048',
            'phone1' => 'required|max:13|unique:users',
            'phone2' => 'nullable|max:13|unique:users',
            'email' => 'required|email|max:100|unique:users',
            'username' => 'required|string|max:50|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code='agt';
            $firstname=$request->input('firstname');
            $lastname=$request->input('lastname');
            $othername=$request->input('othername');
            $gender=$request->input('gender');

            // Handle the user upload of avatar
            if($request->hasFile('user_image')) {
                $path=$request->file('user_image')->store('users_img');
            }else{
                redirect()->back()->with('error','Image Not Found ');
            }

            $phone1=$request->input('phone1');
            $phone2=$request->input('phone2');
            $email=$request->input('email');
            $username=$request->input('username');
            $assembly_id=$request->input('assembly');
            $submetro_id=$request->input('submetro');
            $password=$request->input('password');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'firstname'=>$firstname,//name of region
                'lastname'=>$lastname,//name of region
                'othername'=>$othername,
                'gender'=>$gender,
                'image'=>$path,
                'phone1'=>$phone1,
                'phone2'=>$phone2,
                'email'=>$email,
                'username'=>$username,
                'assembly_id'=>$assembly_id,
                'submetro_id'=>$submetro_id,
                'password' => bcrypt($password),
                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('users')->insert($data);
            $users = User::all();
            return Redirect::to('area-management/user')->with('message','User Successfully Registered! ',['users'=>$users]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showUserForm()
    {
        $assemblies=Assembly::all();
        $submetros=SubMetro::all();
        return view('user-mgmt/create', compact('assemblies'),compact('submetros'));
    }

    //------------------------------------------------------------------

    public function createImageFromBase64(Request $request){
        $file_data = $request->input('picture');
        $file_name = 'image_'.time().'.jpg'; //generating unique file name;
        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);
        if($file_data!=""){ // storing image in storage/app/public Folder
            \Storage::disk('public')->put($file_name,base64_decode($file_data));
        }
    }

    private function gen_uuid()
    {
        return sprintf('-'.'%04x'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
