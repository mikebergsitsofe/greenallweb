<?php

namespace App\Http\Controllers;

use App\Assembly;
use App\SubMetro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SubMetroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submetros = SubMetro::all();
        return view('area-mgmt/submetro/index', ['submetros' => $submetros]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSubMetro($id)
    {
        $submetros = DB::table('submetros')->where('id', $id);
        $submetros->delete();
        return redirect()->back()->with('message', 'Record Successfully Deleted');
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateSubMetro(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            //'name' => 'required|max:60|unique:regions',
            //'prefix_code' => 'required|min:3|unique:regions'
        ], $messages = [
            'name.unique' => $request->input('name').' is already in the System!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $name=$request->input('name');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'name'=>$name,//name of region
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('submetros')->where('id', $id)->update($data);
            $submetros =SubMetro::all();
            return Redirect::to('area-management/submetros')->with('message','Sub-Metro Successfully Updated! ',['submetros'=>$submetros]); //pass your dynamic id
        }
    }

    /**
     * Load Region Update Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateSubMetroForm($id)
    {
        $assemblies = Assembly::all();
        $submetros = DB::table('submetros')->where('submetros.id', $id)
            ->join('assemblies','submetros.assembly_id','=','assemblies.id')
            ->select('submetros.id as id','assemblies.id as assembly_id','submetros.uuid as uuid','submetros.name as submetro_name','assemblies.name as assembly_name')
            ->get();
        $prefix=explode('-',$submetros[0]->uuid);
        $array=array(
            'id'=>$submetros[0]->id,
            'assembly_id'=>$submetros[0]->assembly_id,
            'submetro_name'=>$submetros[0]->submetro_name,
            'assembly_name'=>$submetros[0]->assembly_name,
            'prefix'=>$prefix[0]
        );
        //return $array;
        return view('area-mgmt/submetro/edit',['submetros'=>$array, 'assemblies'=>$assemblies]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function  addSubMetro(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:60|unique:submetros',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
            'name.unique' => $request->input('name').' is already in the System!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $name=$request->input('name');
            $assembly=$request->input('assembly');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for SubMetro
                'name'=>$name,//name of SubMetro
                'assembly_id'=>$assembly,//name of SubMetro
                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('submetros')->insert($data);
            $submetros = SubMetro::all();
            return Redirect::to('area-management/submetros')->with('message','SubMetro Successfully Added! ',['submetros'=>$submetros]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSubMetroForm()
    {
        $assemblies = Assembly::all();
        return view('area-mgmt/submetro/create',compact('assemblies'));
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('-'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
