<?php

namespace App\Http\Controllers;

use App\SubRateCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class FeeFixingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fixed_fees=DB::table('fee_fixings')
            ->join('sub_rate_categories','fee_fixings.sub_rate_category_id','=','sub_rate_categories.id')
            ->join('assemblies','fee_fixings.assembly_id','=','assemblies.id')
            ->select
            (
                'fee_fixings.id as id',
                'fee_fixings.description as description',
                'assemblies.name as assembly',
                'sub_rate_categories.sub_category_name as sub_category',
                'fee_fixings.rate as rate',
                'fee_fixings.rating_year as rating_year'
            )
            ->get();
        //return $fixed_fees;
        return view('app-settings/fees-mgmt/fee-fixing/index', ['fixed_fees' => $fixed_fees]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        //
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  create(Request $request){
        $validator = Validator::make($request->all(), [
            'description' => 'required|max:200',
            'rating_year' => 'required|max:20',
            'rate' => 'required|max:20',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $description=$request->input('description');
            $rate_year=$request->input('rating_year');
            $rating_year=str_split($rate_year,4);
            $rate=$request->input('rate');
            $sub_rate_category_id=$request->input('sub_rate_category_id');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for assembly
                'description'=>$description,
                'rating_year'=>$rating_year[0],
                'rate'=>$rate,
                'sub_rate_category_id'=>$sub_rate_category_id,

                'assembly_id'=>Auth::user()->assembly_id,
                'submetro_id'=>Auth::user()->submetro_id,

                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('fee_fixings')->insert($data);
            $fixed_fees=DB::table('fee_fixings')
                ->join('sub_rate_categories','fee_fixings.sub_rate_category_id','=','sub_rate_categories.id')
                ->join('assemblies','fee_fixings.assembly_id','=','assemblies.id')
                ->select
                (
                    'fee_fixings.id as id',
                    'fee_fixings.description as description',
                    'assemblies.name as assembly',
                    'sub_rate_categories.sub_category_name as sub_category',
                    'fee_fixings.rate as rate',
                    'fee_fixings.rating_year as rating_year'
                )
                ->get();
            return Redirect::to('/app-settings/fees/fixing/fee-mgmt')->with('message','New Fee Successfully Fixed!',['fixed_fees' => $fixed_fees]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        $sub_categories = SubRateCategory::all();
        return view('app-settings/fees-mgmt/fee-fixing/create',['sub_categories' => $sub_categories]);
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('%04x'.'%04x'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
