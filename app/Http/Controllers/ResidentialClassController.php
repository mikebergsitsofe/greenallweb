<?php

namespace App\Http\Controllers;

use App\Assembly;
use App\ResidentialClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ResidentialClassController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residential_classes = ResidentialClass::all();
        return view('app-settings/res-class-mgmt/index', ['residential_classes' => $residential_classes]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteRegion()
    {
        //
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRegion()
    {
        //
    }

    /**
     * Load Region Update Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRegionForm()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function  addResClass(Request $request){
        $validator = Validator::make($request->all(), [
            'description' => 'required|max:60|unique:residential_classes',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
            'description.unique' => $request->input('description').' is already in the System!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $assembly_code=Assembly::all()->where('id',Auth::user()->assembly_id)->pluck('name');
            $code=substr($assembly_code,2,4);
            $desc=$request->input('description');
            $rating_year=$request->input('rating_year');
            $rate=$request->input('rate');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'description'=>$desc,//class name
                'rating_year'=>$rating_year,//rating year
                'rate'=>$rate,//rate amount

                'assembly_id' => Auth::user()->assembly_id,//current user id is stored with the transaction as created by
                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('residential_classes')->insert($data);
            $residential_classes = ResidentialClass::all();
            return Redirect::to('app-settings/res-class-mgmt')->with('message','Successfully Added!',['residential_classes' => $residential_classes]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showResClassForm()
    {
        $assemblies=Assembly::all()->where('id',Auth::user()->assembly_id);
        return view('app-settings/res-class-mgmt/create',['assemblies'=> $assemblies]);
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('%04x'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
