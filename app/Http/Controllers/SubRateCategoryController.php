<?php

namespace App\Http\Controllers;

use App\MainRateCategory;
use App\SubRateCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SubRateCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_categories=DB::table('sub_rate_categories')
            ->join('main_rate_categories','sub_rate_categories.main_rate_category_id','=','main_rate_categories.id')
            ->join('assemblies','sub_rate_categories.assembly_id','=','assemblies.id')
            ->select
            (
                'sub_rate_categories.id as id',
                'assemblies.name as assembly',
                'sub_rate_categories.sub_category_name as sub_category',
                'main_rate_categories.main_category_name as main_category'
            )
            ->get();
        //return $sub_categories;
        return view('app-settings/fees-mgmt/sub-category/index', ['sub_categories' => $sub_categories]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        //
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  create(Request $request){
        $validator = Validator::make($request->all(), [
            'sub_category_name' => 'required|max:100|unique:sub_rate_categories',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
            'sub_category_name.unique' => $request->input('sub_category_name').' is already in the System!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $sub_category_name=$request->input('sub_category_name');
            $main_rate_category_id=$request->input('main_rate_category_id');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for assembly
                'sub_category_name'=>$sub_category_name,
                'main_rate_category_id'=>$main_rate_category_id,

                'assembly_id'=>Auth::user()->assembly_id,
                'submetro_id'=>Auth::user()->submetro_id,

                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('sub_rate_categories')->insert($data);
            $sub_categories=DB::table('sub_rate_categories')
                ->join('main_rate_categories','sub_rate_categories.main_rate_category_id','=','main_rate_categories.id')
                ->join('assemblies','sub_rate_categories.assembly_id','=','assemblies.id')
                ->select
                (
                    'sub_rate_categories.id as id',
                    'assemblies.name as assembly',
                    'sub_rate_categories.sub_category_name as sub_category',
                    'main_rate_categories.main_category_name as main_category'
                )
                ->get();
            return Redirect::to('/app-settings/sub/categories/fee-mgmt')->with('message','Main Category Successfully Added!',['sub_categories' => $sub_categories]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        $main_categories = MainRateCategory::all();
        return view('app-settings/fees-mgmt/sub-category/create',['main_categories' => $main_categories]);
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('%04x'.'%04x'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
