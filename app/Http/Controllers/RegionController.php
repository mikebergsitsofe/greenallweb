<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class RegionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::all();
        return view('area-mgmt/region/index', ['regions' => $regions]);
    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteRegion($id)
    {
        $region = DB::table('regions')->where('id', $id);
        $region->delete();
        return redirect()->back()->with('message', 'Record Successfully Deleted');
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRegion(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            //'name' => 'required|max:60|unique:regions',
            //'prefix_code' => 'required|min:3|unique:regions'
        ], $messages = [
            'name.unique' => $request->input('name').' is already in the System!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $name=$request->input('name');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'name'=>$name,//name of region
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('regions')->where('id', $id)->update($data);
            $regions = Region::all();
            return Redirect::to('area-management/regions')->with('message','Region Successfully Updated! ', ['regions' => $regions]); //pass your dynamic id
        }
    }

    /**
     * Load Region Update Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRegionForm($id)
    {
        $regions = DB::table('regions')->where('id', $id)->get();
        $prefix=explode('-',$regions[0]->uuid);
        $array=array(
            'id'=>$regions[0]->id,
            'name'=>$regions[0]->name,
            'prefix'=>$prefix[0]
        );
        //return $array;
        return view('area-mgmt/region/edit',['regions'=>$array]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function  addRegion(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:60|unique:regions',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
                'name.unique' => $request->input('name').' is already in the System!',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $code=$request->input('prefix_code');
            $name=$request->input('name');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'name'=>$name,//name of region
                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('regions')->insert($data);
            $regions = Region::all();
            return Redirect::to('area-management/regions')->with('message','Region Successfully Added! ', ['regions' => $regions]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegionForm()
    {
        return view('area-mgmt/region/create');
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('-'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
