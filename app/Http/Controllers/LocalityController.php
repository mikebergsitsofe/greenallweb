<?php

namespace App\Http\Controllers;

use App\Assembly;
use App\Locality;
use App\ResidentialClass;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LocalityController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getUserAssembly(){
        $user_assembly=Auth::user()->assembly_id;
        return $user_assembly;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getUserSubMetro(){
        $user_submentro=Auth::user()->submetro_id;
        return $user_submentro;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localities = DB::table('localities')
            ->leftJoin('residential_classes', 'localities.residential_class_id', '=', 'residential_classes.id')
            ->get();

        return view('app-settings/locality-mgmt/index', ['localities' => $localities]);


    }

    /**
     * Delete Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteLocality()
    {
        //
    }

    /**
     * Update Region by id.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateLocality()
    {
        //
    }

    /**
     * Load Region Update Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateLocalityForm()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function  addLocality(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:60',
            //'prefix_code' => 'required|max:3|unique:regions'
        ], $messages = [
            'name.required' => $request->input('name').' is a Required Field!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            //Get form input fields values
            $assembly_code=Assembly::all()->where('assembly_id',$this->getUserAssembly())->pluck('name');
            $code=substr($assembly_code,2,4);
            $name=$request->input('name');
            $residential_class_id=$request->input('residential_class_id');

            //add unique generated codes to user codes(prefix+uuid)
            $uuid=strtoupper($code.$this->gen_uuid());
            //Prepared Input data for db storage
            $data=array(
                'uuid'=>$uuid,//unique for region
                'name'=>$name,//class name
                'residential_class_id'=>$residential_class_id,//class name

                'submetro_id' => $this->getUserSubMetro(),//current user id is stored with the transaction as created by
                'assembly_id' => $this->getUserAssembly(),//current user id is stored with the transaction as created by

                'created_by' => Auth::user()->id,//current user id is stored with the transaction as created by
                'updated_by' => Auth::user()->id,//current user id is stored with the transaction as updated by
                'created_at' => Carbon::now(),//Server timestamp
                'updated_at' => Carbon::now()//Server timestamp
            );
            DB::table('localities')->insert($data);
            $localities = DB::table('localities')
                ->leftJoin('residential_classes', 'localities.residential_class_id', '=', 'residential_classes.id')
                ->get();
            return Redirect::to('app-settings/locality-mgmt')->with('message','Successfully Added!',['localities' => $localities]); //pass your dynamic id
        }
    }

    /**
     * Load Region Add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLocalityForm()
    {
        $residential_classes=DB::table('residential_classes')->select('id','description')->where('assembly_id',$this->getUserAssembly())->get();
        return view('app-settings/locality-mgmt/create',compact('residential_classes'));
    }

    //------------------------------------------------------------------
    private function gen_uuid()
    {
        return sprintf('%04x'.'%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
