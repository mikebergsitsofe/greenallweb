<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResidentialClass extends Model
{
    public function property()
    {
        return $this->hasMany('localities');
    }
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'residential_classes';


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
